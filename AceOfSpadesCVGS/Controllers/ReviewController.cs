﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceOfSpadesCVGS.Models;
using Microsoft.AspNet.Identity;

namespace AceOfSpadesCVGS.Controllers
{
    public class ReviewController : Controller
    {
        private cvgsEntities1 db = new cvgsEntities1();

        // GET: Review
        public ActionResult Add(int? id)
        {
            Session["UserId"] = id;
            return View("Add");
        }
        // POST: Review/Add
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "reviewId,reviewDescription,gameId,userId,isApproved,ratingAmount")] CVGS_reviews cVGS_reviews)
        {
            if (ModelState.IsValid)
            {
                cVGS_reviews.isApproved = false;
                string userId = User.Identity.GetUserId();
                cVGS_reviews.userId = userId;
                if (Session["UserId"] != null)
                {
                    //Converting your session variable value to integer
                    int session = Convert.ToInt32(Session["UserId"]);
                    cVGS_reviews.gameId = session;
                }
                else
                {
                    return View("Index");
                }
                db.CVGS_reviews.Add(cVGS_reviews);
                db.SaveChanges();
                return RedirectToAction("Pending");
            }

            return View(cVGS_reviews);
        }
        // GET: Pending
        public ActionResult Pending()
        {
            return View("");
        }
        // GET: ViewPending
        public ActionResult ViewPending()
        {
            string userId = User.Identity.GetUserId();

            return View(db.CVGS_reviews.Where(r => r.userId == userId && !r.isApproved).ToList());
        }

        // GET: ViewPending
        public ActionResult EmployeeReview()
        {
            return View(db.CVGS_reviews.Where(r => !r.isApproved).ToList());
        }


        // GET: Review/Details/5
        public ActionResult Details(int? id)
        {
            var newId = db.CVGS_reviews.Where(i => i.gameId == id).ToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (newId == null)
            {
                return HttpNotFound();
            }
            var ratings = db.CVGS_reviews.Where(r => r.gameId == id && r.isApproved == true);
            var count = ratings.Count();
            var sum = ratings.Sum(s => s.ratingAmount);
            ViewBag.Average = sum / count;
            return View(ratings.ToList());
        }

        //    // GET: Review/Create
        //    public ActionResult Create()
        //    {
        //        return View();
        //    }

        //// POST: Review/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "reviewId,reviewDescription,gameId,userId,isApproved")] CVGS_reviews cVGS_reviews)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.CVGS_reviews.Add(cVGS_reviews);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(cVGS_reviews);
        //}

        // GET: Review/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_reviews cVGS_reviews = db.CVGS_reviews.Find(id);
            if (cVGS_reviews == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_reviews);
        }

        // POST: Review/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "reviewId,reviewDescription,gameId,userId,isApproved,ratingAmount")] CVGS_reviews cVGS_reviews)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cVGS_reviews).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("EmployeeReview");
            }
            return View(cVGS_reviews);
        }

        // GET: Review/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_reviews cVGS_reviews = db.CVGS_reviews.Find(id);
            if (cVGS_reviews == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_reviews);
        }

        // POST: Review/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CVGS_reviews cVGS_reviews = db.CVGS_reviews.Find(id);
            db.CVGS_reviews.Remove(cVGS_reviews);
            db.SaveChanges();
            return RedirectToAction("EmployeeReview");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
