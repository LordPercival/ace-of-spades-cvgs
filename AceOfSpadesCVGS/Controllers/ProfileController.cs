﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AceOfSpadesCVGS.Models;
using System.Data.Entity.Validation;
using Microsoft.AspNet.Identity;

namespace AceOfSpadesCVGS.Controllers
{
    public class ProfileController : Controller
    {
        private cvgsEntities1 db = new cvgsEntities1();

        // GET: Profile
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                string userId = User.Identity.GetUserId();
                try
                {
                    var cVGS_member = db.CVGS_member.Include(c => c.AspNetUser).Include(c => c.CVGS_address).Single(c => c.user_id == userId);
                    return View(cVGS_member);
                }
                catch
                {
                    return RedirectToAction("Create");
                }
            }
            else
            {
                try
                {
                    var cVGS_member = db.CVGS_member.Include(c => c.AspNetUser).Include(c => c.CVGS_address).Single(c => c.Id == id);
                    return View(cVGS_member);
                }
                catch
                {
                    return RedirectToAction("Create");
                }
            }
        }

        // GET: Profile/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_member cVGS_member = await db.CVGS_member.FindAsync(id);
            if (cVGS_member == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_member);
        }

        // GET: Profile/Create
        public ActionResult Create()
        {
            ViewBag.user_id = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.Id = new SelectList(db.CVGS_address, "address_id", "address_street");
            return View();
        }

        // POST: Profile/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,name,platform,game_type,user_id,email,gender,DOB,shipping_address,mailling_address,creditCardNumber,expiryDate,creditCardVerfiyNumber")] CVGS_member cVGS_member)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    string userId = User.Identity.GetUserId();
                    string email = db.AspNetUsers.Single(u => u.Id == userId).Email;
                    cVGS_member.user_id = userId;
                    cVGS_member.email = email;
                    db.CVGS_member.Add(cVGS_member);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting  
                            // the current instance as InnerException  
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }

            ViewBag.user_id = new SelectList(db.AspNetUsers, "Id", "Email", cVGS_member.user_id);
            ViewBag.Id = new SelectList(db.CVGS_address, "address_id", "address_street", cVGS_member.Id);
            return View(cVGS_member);
        }

        // GET: Profile/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_member cVGS_member = await db.CVGS_member.FindAsync(id);
            if (cVGS_member == null)
            {
                return HttpNotFound();
            }
            ViewBag.user_id = new SelectList(db.AspNetUsers, "Id", "Email", cVGS_member.user_id);
            ViewBag.Id = new SelectList(db.CVGS_address, "address_id", "address_street", cVGS_member.Id);
            ViewBag.Id = new SelectList(db.CVGS_library, "member_id", "game_id", cVGS_member.Id);
            return View(cVGS_member);
        }

        // POST: Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,name,platform,game_type,user_id,email,gender,DOB,shipping_address,mailling_address,creditCardNumber,expiryDate,creditCardVerfiyNumber")] CVGS_member cVGS_member)
        {
            if (ModelState.IsValid)
            {
                CVGS_member memberToEdit = db.CVGS_member.Single(m => m.user_id == cVGS_member.user_id);
                memberToEdit.Id = cVGS_member.Id;
                memberToEdit.name = cVGS_member.name;
                memberToEdit.platform = cVGS_member.platform;
                memberToEdit.game_type = cVGS_member.game_type;
                //memberToEdit.user_id = cVGS_member.user_id;
                //memberToEdit.name = cVGS_member.email;
                memberToEdit.gender = cVGS_member.gender;
                memberToEdit.DOB = cVGS_member.DOB;
                memberToEdit.shipping_address = cVGS_member.shipping_address;
                memberToEdit.mailling_address = cVGS_member.mailling_address;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = memberToEdit.Id});
            }
            ViewBag.user_id = new SelectList(db.AspNetUsers, "Id", "Email", cVGS_member.user_id);
            ViewBag.Id = new SelectList(db.CVGS_address, "address_id", "address_street", cVGS_member.Id);
            ViewBag.Id = new SelectList(db.CVGS_library, "member_id", "game_id", cVGS_member.Id);
            return View(cVGS_member);
        }

        // GET: Profile/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CVGS_member cVGS_member = await db.CVGS_member.FindAsync(id);
            if (cVGS_member == null)
            {
                return HttpNotFound();
            }
            return View(cVGS_member);
        }

        // POST: Profile/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CVGS_member cVGS_member = await db.CVGS_member.FindAsync(id);
            db.CVGS_member.Remove(cVGS_member);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
