﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AceOfSpadesCVGS.Startup))]
namespace AceOfSpadesCVGS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
