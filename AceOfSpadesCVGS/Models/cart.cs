﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AceOfSpadesCVGS.Models
{
    public class Cart
    {
       public CVGS_game CVGS_Game
        {
            get;
            set;
        }
        public int Quantity
        {
            get;
            set;
        }
        public Cart(CVGS_game CVGS_game,int quantity )
        {
            CVGS_Game = CVGS_game;
            Quantity = quantity;
        }
    }
}