﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AceOfSpadesCVGS;
using AceOfSpadesCVGS.Controllers;

namespace AceOfSpadesCVGS.Tests.Controllers
{

    [TestClass]
    public class ReportControllerTest
    {
        [TestMethod]
        public void ReportIndexView()
        {
            // Arrange
            ReportController controller = new ReportController();
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void MemberListReportView()
        {
            // Arrange
            ReportController controller = new ReportController();
            // Act
            ViewResult result = controller.MemberListReport() as ViewResult;
            // Assert
            Assert.AreEqual("MemberListReport", result.ViewName);
        }
        [TestMethod]
        public void MemberDetailReportView()
        {
            // Arrange
            ReportController controller = new ReportController();
            // Act
            ViewResult result = controller.MemberDetailReport() as ViewResult;
            // Assert
            Assert.AreEqual("MemberDetailReport", result.ViewName);
        }
        [TestMethod]
        public void SalesReportView()
        {
            // Arrange
            ReportController controller = new ReportController();
            // Act
            ViewResult result = controller.SalesReport() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
    }
}    