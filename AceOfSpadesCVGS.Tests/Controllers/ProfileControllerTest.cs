﻿using AceOfSpadesCVGS.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AceOfSpadesCVGS.Tests.Controllers
{
    [TestClass]
    public class ProfileControllerTest
    {
        [TestMethod]
        public void CreateProfile_View()
        {
            // Arrange
            ProfileController controller = new ProfileController();
            // Act
            ViewResult result = controller.Create() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }
        
    }
}
