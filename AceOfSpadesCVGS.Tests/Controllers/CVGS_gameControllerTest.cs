﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AceOfSpadesCVGS;
using AceOfSpadesCVGS.Controllers;

namespace AceOfSpadesCVGS.Tests.Controllers
{
    [TestClass]
    public class CVGS_gameControllerTest
    {
        [TestMethod]
        public void CreateCVGS_View()
        {
            // Arrange
            CVGS_gameController controller = new CVGS_gameController();
            // Act
            ViewResult result = controller.Create() as ViewResult;
            // Assert
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void DetailsCVGS_ImpossibleId()
        {
            // Arrange
            CVGS_gameController controller = new CVGS_gameController();
            int myInt = 5;
            myInt = myInt * -1;
            // Act
            RedirectToRouteResult result = (RedirectToRouteResult)controller.Details(myInt);
            // Assert
            Assert.AreEqual("Index", result.RouteValues["action"]);
        }
        [TestMethod]
        public void EditCVGS_IdNullStatusCode()
        {
            // Arrange
            CVGS_gameController controller = new CVGS_gameController();
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.Details(null);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }
        [TestMethod]
        public void DeleteCVGS_IdNullStatusCode()
        {
            // Arrange
            CVGS_gameController controller = new CVGS_gameController();
            // Act
            HttpStatusCodeResult result = (HttpStatusCodeResult)controller.Details(null);
            // Assert
            Assert.AreEqual(400, result.StatusCode);
        }
        
    }
}